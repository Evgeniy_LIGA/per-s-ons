# Краткое описание API 

Метод для реализации задания 1. Определить существует ли хоть одно совпадение с шаблона в коллекции.
    
    boolean hasDuplicateOf(List<Person> list, Person p);

Метод для реализации задания 2. Вывести людей, у которых возраст не превышает 20 лет и фамилия начинается
на одну из букв (а,б,в,г,д) как в нижнем, так и верхнем регистре.

    List<Person> filteredByAgeLEQAndFirstLetterEQ(List<Person> list, int ageLEQ, char... chars );

Метод для реализации задания 3. Вывести повторяющиеся объекты в следующем виде {1:{Объект человека}}
в порядке убывания возраста человека (1-количество повторяющихся элементов). 
    
    Map<Person,Long> groupedByNumberOfRepeatsAndSortedByAgeDescending(List<Person> list);

Метод вывода данных для задания 2

    void show(List<Person> list);

Метод вывода данных для задания 3

    void showWithRepeats(Map<Person,Long> list);

# Автоматическое тестирование JUnit

Класс CoreTest модуля core содержит несколько автоматических методов тестирования

Проверка функции реализующей выполнение задания 1

    hasDuplicateOf()

Проверка функции реализующей выполнение задания 2

    filteredByAgeLEQAndFirstLetterEQ()

Проверка функции реализующей выполнение задания 3

    groupedByNumberOfRepeatsAndSortedByAgeDescending() 

# Ручное тестирование

Модуль core содержит класс ManualTest предназначенный для визуальной проверки 
реализации заданий сервиса.

## Подготовка

 На данном этапе создается массив случайных персон с добавлением дубликатов.

        // Создаем новый список персон
        List<Person> persons = new ArrayList<>();
        // Наполняем его 8 случайно сгенерированными персонами
        PersonGenerator.generateRandomArray(persons,8);
        // Намеренно для проверки создаем 4 дубликата
        PersonGenerator.addDublicates(persons,4);
        // Создаем объект нашего сервиса
        Core core = new Core();
        // Отображаем сгенерированный список персон
        System.out.println("Исходный список персон\r\n");
        core.show(persons);

Результат:

    Input person Collection:
    Person{surname='ветров', name='Степан', middle name='Иванович', age=61, sex=male, born date=1959-05-12}
    Person{surname='сидоров', name='Алик', middle name='Петрович', age=17, sex=male, born date=2004-01-05}
    Person{surname='ветров', name='Петр', middle name='Степанович', age=41, sex=male, born date=1979-12-11}
    Person{surname='Сидорова', name='Иванна', middle name='Степановна', age=68, sex=female, born date=1952-10-15}
    Person{surname='петрова', name='Дианна', middle name='Евгеньевна', age=43, sex=female, born date=1977-11-04}
    Person{surname='Ежов', name='Иван', middle name='Петрович', age=70, sex=male, born date=1950-06-08}
    Person{surname='быкова', name='Евгения', middle name='Васильевна', age=12, sex=female, born date=2008-09-27}
    Person{surname='ветров', name='Дмитрий', middle name='Петрович', age=80, sex=male, born date=1941-01-19}
    Person{surname='ветров', name='Степан', middle name='Иванович', age=61, sex=male, born date=1959-05-12}
    Person{surname='быкова', name='Евгения', middle name='Васильевна', age=12, sex=female, born date=2008-09-27}
    Person{surname='сидоров', name='Алик', middle name='Петрович', age=17, sex=male, born date=2004-01-05}
    Person{surname='ветров', name='Степан', middle name='Иванович', age=61, sex=male, born date=1959-05-12}

## Тест 1. Проверка на дубликаты

Первый тест. Подсовываем дубликат сервису и вызываем core.hasDuplicateOf метод.
Выводим результат на экран. Результат должен быть как дубликат найден.

        Person duplicatePerson = persons.get(0);
        System.out.println("Collection have duplicate of this ⤵ person ?\r\n"+duplicatePerson);
        System.out.println("Result = "+core.hasDuplicateOf(persons,duplicatePerson));
        System.out.println();

Результат:

    Collection have duplicate of this ⤵ person ?
    Person{surname='ветров', name='Степан', middle name='Иванович', age=61, sex=male, born date=1959-05-12}
    Result = true

Далее подсовываем уникального случайно сгенерированного персонажа 
и вызываем core.hasDuplicateOf метод. Результат должен быть как дубликат не найден.

        Person randomPerson = PersonGenerator.generate_Random();
        System.out.println("Collection have duplicate of this ⤵ person ?\r\n"+randomPerson);
        System.out.println("Result = "+core.hasDuplicateOf(persons,randomPerson));
        System.out.println();

Результат:

    Collection have duplicate of this ⤵ person ?
    Person{surname='Ветров', name='Дмитрий', middle name='Васильевич', age=47, sex=male, born date=1973-07-04}
    Result = false

## Шаг 2. Выборка из массива персон

Задание 2. Вывести людей, у которых возраст не превышает 20 лет и фамилия начинается на одну
из букв (а,б,в,г,д) как в нижнем, так и верхнем регистре, реализуется методом
core.filteredByAgeLEQAndFirstLetterEQ() вывод осуществляется методом core.show()

        System.out.println("Collection of persons whose age is <= 20 and "+
                "with surname whose first letter is equal to 'а','б','в','г','д' in any char case ⤵");
        List<Person> task1_collection = core.filteredByAgeLEQAndFirstLetterEQ(persons,
                20,'а','б','в','г','д');
        core.show(task1_collection);
        System.out.println();

Результат:

    Collection of persons whose age is <= 20 and with surname whose first letter is equal to 'а','б','в','г','д' in any char case ⤵
    Person{surname='быкова', name='Евгения', middle name='Васильевна', age=12, sex=female, born date=2008-09-27}
    Person{surname='быкова', name='Евгения', middle name='Васильевна', age=12, sex=female, born date=2008-09-27}

## Шаг 3. Группирование элементов, подсчет повторов и сортировка

Задание 3. Вывести повторяющиеся объекты в следующем виде {1:{Объект человека}}
в порядке убывания возраста человека. (1-количество повторяющихся элементов)
реализуется методом groupedByNumberOfRepeatsAndSortedByAgeDescending() 
вывод осуществляется методом core.showWithRepeats()

        System.out.println("Collection of persons grouped by repeats, and sorted by age " +
                " descending and showed as {1:{Person object}} "+
                " where (1 - is quantity of repeated elements");
        Map<Person, Long> task2_map = core.groupedByNumberOfRepeatsAndSortedByAgeDescending(persons);
        core.showWithRepeats(task2_map);

Результат:

    Collection of persons grouped by repeats, and sorted by age  descending and showed as {1:{Person object}}  where (1 - is quantity of repeated elements
    {1:Person{surname='ветров', name='Дмитрий', middle name='Петрович', age=80, sex=male, born date=1941-01-19}]
    {1:Person{surname='Ежов', name='Иван', middle name='Петрович', age=70, sex=male, born date=1950-06-08}]
    {1:Person{surname='Сидорова', name='Иванна', middle name='Степановна', age=68, sex=female, born date=1952-10-15}]
    {3:Person{surname='ветров', name='Степан', middle name='Иванович', age=61, sex=male, born date=1959-05-12}]
    {1:Person{surname='петрова', name='Дианна', middle name='Евгеньевна', age=43, sex=female, born date=1977-11-04}]
    {1:Person{surname='ветров', name='Петр', middle name='Степанович', age=41, sex=male, born date=1979-12-11}]
    {2:Person{surname='сидоров', name='Алик', middle name='Петрович', age=17, sex=male, born date=2004-01-05}]
    {2:Person{surname='быкова', name='Евгения', middle name='Васильевна', age=12, sex=female, born date=2008-09-27}]

