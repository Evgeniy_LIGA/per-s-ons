import java.time.LocalDate;
import java.util.Objects;

public class Person implements Cloneable {
    public String surname;
    public String name;
    public String middle_name;
    public int age;
    public Sex sex;
    public LocalDate born_date;

    /** Constructor of Template */
    public Person(String surname, String name, String middle_name, int age, Sex sex, LocalDate born_date){
        this.surname = surname;
        this.name = name;
        this.middle_name = middle_name;
        this.age = age;
        this.sex = sex;
        this.born_date = born_date;
    }

    /** Constructor of EMPTY object  */
    public Person(){
        this.name = "";
        this.surname = "";
        this.middle_name = "";
        this.age = 0;
        this.sex = Sex.male;
        this.born_date = LocalDate.now();
    }

    public Person clone(){
        Person p = new Person();
        p.name = this.name;
        p.surname = this.surname;
        p.middle_name = this.middle_name ;
        p.age = this.age;
        p.sex = this.sex;
        p.born_date = this.born_date;
        return p;
    }

    /** Специфический геттер для задания */
    public boolean getSurnameFirstLetterEQ(char... chars ) {
        char fc = this.surname.toLowerCase().charAt(0);
        for(char c : chars)
            if(fc==c)return true;
        return false;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public int getAge() {
        return age;
    }

    public Sex getSex() {
        return sex;
    }

    public LocalDate getBorn_date() {
        return born_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name) && Objects.equals(surname, person.surname) && Objects.equals(middle_name, person.middle_name) && sex == person.sex && Objects.equals(born_date, person.born_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, middle_name, age, sex, born_date);
    }

    @Override
    public String toString() {
        return "Person{" +
                "surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", middle name='" + middle_name + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                ", born date=" + born_date +
                //", hashCode=" + this.hashCode() +
                '}';
    }
}
