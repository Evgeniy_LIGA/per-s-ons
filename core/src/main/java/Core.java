import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Core implements Services{

    @Override
    public void show(List<Person> list){
        for (Person p : list)
            System.out.println(p.toString());
    }

    @Override
    public void showWithRepeats(Map<Person, Long> list) {
        list.entrySet().stream()
                .forEach(map->System.out.println("{"+map.getValue()+":"+map.getKey()+"]"));
    }

    /** Метод для реализации задания 1.
     * Определить существует ли хоть одно совпадение с шаблона в коллекции.*/
    @Override
    public boolean hasDuplicateOf(List<Person> list, Person p){
        if(list.contains(p)) return true;
        return false;
    }

    /** Метод для реализации задания 2.
     * Вывести людей, у которых возраст не превышает 20 лет и фамилия начинается
     * на одну из букв (а,б,в,г,д) как в нижнем, так и верхнем регистре.*/
    @Override
    public List<Person> filteredByAgeLEQAndFirstLetterEQ(List<Person> list, int ageLEQ, char... chars ){
        return list.stream()
                .filter((Person p) -> {
                    return p.getAge() <= ageLEQ && p.getSurnameFirstLetterEQ(chars);
                })
                .collect(Collectors.toList());
    }

    /** Метод для реализации задания 3.
     *  Вывести повторяющиеся объекты в следующем виде {1:{Объект человека}}
     *  в порядке убывания возраста человека. (1-количество повторяющихся элементов) */
    @Override
    public Map<Person, Long> groupedByNumberOfRepeatsAndSortedByAgeDescending(List<Person> list) {
        // Группируем повторяющиеся значения, количество повторов записываем в Map.Value (Long)
        Map<Person, Long> task2 =list.stream().collect(
                Collectors.groupingBy(
                        Function.identity(),
                        HashMap::new,
                        Collectors.counting()
                ));

        // Сортируем список по возрасту и сохраняем Map<Person, Long>
        // если -a.getKey().getAge() минус значит в ратном порядке
        task2 = task2.entrySet().stream().
                sorted((b,a)->a.getKey().getAge()-b.getKey().getAge()).
                collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (old_value,new_value)->old_value,
                        LinkedHashMap::new
                ));
        return task2;
    }
}
