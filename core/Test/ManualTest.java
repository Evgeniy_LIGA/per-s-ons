import org.junit.Test;

import java.util.*;

public class ManualTest {

    @Test
    public void Manual_Test() {

    // Создаем новый список персон
        List<Person> persons = new ArrayList<>();
        // Наполняем его 8 случайно сгенерированными персонами
        PersonGenerator.generateRandomArray(persons,8);
        // Намеренно для проверки создаем 4 дубликата
        PersonGenerator.addDuplicates(persons,4);
        // Создаем объект нашего сервиса
        Core core = new Core();
        // Отображаем сгенерированный список персон
        System.out.println("Input person Collection:\r\n");
        core.show(persons);
        System.out.println();

        /** Задание 1. Определить существует ли хоть одно совпадение с шаблона в коллекции.*/
        // Тестируем на дублирование намеренно подставив дубликат
        Person duplicatePerson = persons.get(0);
        System.out.println("Collection have duplicate of this ⤵ person ?\r\n"+duplicatePerson);
        System.out.println("Result = "+core.hasDuplicateOf(persons,duplicatePerson));
        System.out.println();

        // Тестируем на дублирование подставив случайную сгенерированную персону
        Person randomPerson = PersonGenerator.generate_Random();
        System.out.println("Collection have duplicate of this ⤵ person ?\r\n"+randomPerson);
        System.out.println("Result = "+core.hasDuplicateOf(persons,randomPerson));
        System.out.println();

        /** Задание 2. Вывести людей, у которых возраст не превышает 20 лет и фамилия начинается на одну
         из букв (а,б,в,г,д) как в нижнем, так и верхнем регистре.*/
        System.out.println("Collection of persons whose age is <= 20 and "+
                "with surname whose first letter is equal to 'а','б','в','г','д' in any char case ⤵");
        List<Person> task1_collection = core.filteredByAgeLEQAndFirstLetterEQ(persons,
                20,'а','б','в','г','д');
        core.show(task1_collection);
        System.out.println();

        /** Задание 3. Вывести повторяющиеся объекты в следующем виде {1:{Объект человека}}
         в порядке убывания возраста человека (1-количество повторяющихся элементов). */
        System.out.println("Collection of persons grouped by repeats, and sorted by age " +
                " descending and showed as {1:{Person object}} "+
                " where (1 - is quantity of repeated elements");
        Map<Person, Long> task2_map = core.groupedByNumberOfRepeatsAndSortedByAgeDescending(persons);
        core.showWithRepeats(task2_map);
    }

}
