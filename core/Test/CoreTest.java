import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CoreTest {

    static int num = 1;

    @Before
    public void runBeforeEveryTest() {
        System.out.println("Run test №"+num+"...");
    }

    @After
    public void runAfterEveryTest() {
        System.out.println("Test №"+num+" completed!");
        num++;
    }

    Person ivan = new Person ("Иванов","Иван","Иванович",17,Sex.male, LocalDate.of(2004,1,5));
    Person stepan = new Person ("Степанов","Степан","Степанович",41,Sex.male, LocalDate.of(1979,12,11));
    Person anna = new Person ("Сидорова","Анна","Алексеевна",12,Sex.female, LocalDate.of(2008,9,27));
    Person nina = new Person ("Быкова","Нина","Ивановна",17,Sex.female, LocalDate.of(2004,1,5));
    Person kseniya = new Person ("гусева","Ксения","Алексеевна",12,Sex.female, LocalDate.of(2008,9,27));

    /** JUnit тест для метода реализующего задание 1 */
    @Test
    public void hasDuplicateOf() {
        Core core = new Core();
        // Подготавливаем входные данные
        // Чтобы объекты были не по ссылке на instance используем clone()
        List<Person> input = new ArrayList<>();
        // Добавляем
        input.add(ivan.clone());
        input.add(kseniya.clone());
        input.add(stepan.clone());
        input.add(nina.clone());
        // В списке Анны нет. Проверим это.
        boolean result;
        result = core.hasDuplicateOf(input,anna.clone());
        assertEquals(result, false);

        // Теперь добавим Анну. И еще несколько человек и проведем тест заново.
        input.add(anna.clone());
        input.add(kseniya.clone());
        input.add(stepan.clone());
        input.add(nina.clone());
        result = core.hasDuplicateOf(input,anna.clone());
        assertEquals(result, true);
    }

    /** JUnit тест для метода реализующего задание 2 */
    @Test
    public void filteredByAgeLEQAndFirstLetterEQ() {
        Core core = new Core();
        // Подготавливаем входные данные
        List<Person> input = new ArrayList<>();
        // Добавляем
        input.add(ivan);
        input.add(kseniya);
        input.add(stepan);
        input.add(nina);
        input.add(kseniya);

        List<Person> output =  core.filteredByAgeLEQAndFirstLetterEQ(input,20,'а','б','в','г','д');
        // Ожидаемый результат
        // Person{surname='гусева', name='Ксения', middle name='Алексеевна', age=12, sex=female, born date=2008-09-27}
        // Person{surname='Быкова', name='Нина', middle name='Ивановна', age=17, sex=female, born date=2004-01-05}
        // Person{surname='гусева', name='Ксения', middle name='Алексеевна', age=12, sex=female, born date=2008-09-27}
        // Подготовим его
        List<Person> result = new ArrayList<>();
        result.add(kseniya);
        result.add(nina);
        result.add(kseniya);

        // Тестируем сравнением значений
        assertEquals(output, result);
    }

    /** JUnit тест для метода реализующего задание 3 */
    @Test
    public void groupedByNumberOfRepeatsAndSortedByAgeDescending() {
        Core core = new Core();
        // Подготавливаем входные данные
        List<Person> input = new ArrayList<>();
        // Добавляем 3 Ивана 17 лет, 2 Степана 41 год, и одну Анну 12 лет.
        input.add(ivan);
        input.add(stepan);
        input.add(ivan);
        input.add(stepan);
        input.add(anna);
        input.add(ivan);
        // Применяем метод
        Map<Person, Long> output = core.groupedByNumberOfRepeatsAndSortedByAgeDescending(input);

        // Ожидаемый результат
        // {2:Person{surname='Степанов', name='Степан', middle name='Степанович', age=41, sex=male, born date=1979-12-11}]
        // {3:Person{surname='Иванов', name='Иван', middle name='Иванович', age=17, sex=male, born date=2004-01-05}]
        // {1:Person{surname='Сидорова', name='Анна', middle name='Алексеевна', age=12, sex=female, born date=2008-09-27}]
        // Подготовим его
        Map<Person, Long> result = new HashMap<>();
        result.put(stepan,(long)2);
        result.put(ivan,(long)3);
        result.put(anna,(long)1);

        // Тестируем сравнением значений
        assertEquals(output, result);
    }
}
