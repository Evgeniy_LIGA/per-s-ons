import java.time.LocalDate;
import java.time.Period;
import java.util.Random;
import java.util.List;

/** Класс содержит в себе утилиты для работы с DTO Person.
 *  Например генерацию случайной персоны */
public class PersonGenerator {

    public static Random rnd = new Random(777);
    public static String[] sn = new String[]{"Иванов","Петров","Сидоров","Лебедев",
            "Андреев","Быков","Ветров","Гусев","Демин","Ежов"};
    public static String[] mn = new String[]{"Иван","Петр","Евгений","Степан","Юлиан","Алексей","Дмитрий"};
    public static String[] fn = new String[]{"Инга","Полина","Евгения","Стелла","Юлия","Анна","Дианна"};
    public static String[] mmn = new String[]{"Иванович","Петрович","Евгеньевич","Степанович","Поликарпович","Васильевич"};
    public static String[] fmn = new String[]{"Ивановна","Петровна","Евгеньевна","Степановна","Юлиановна","Васильевна"};

    public static Person generate_Random(){
        Person person;
        person = new Person();
        person.sex = (rnd.nextBoolean()==true? Sex.male: Sex.female);
        person.surname = sn[rnd.nextInt(sn.length)];
        if(person.sex == Sex.female){
            person.surname +="а";
            person.name = fn[rnd.nextInt(fn.length)];
            person.middle_name = fmn[rnd.nextInt(fmn.length)];
        } else {
            person.name = mn[rnd.nextInt(mn.length)];
            person.middle_name = mmn[rnd.nextInt(mmn.length)];
        }
        if(rnd.nextBoolean()==true)person.surname = person.surname.toLowerCase();
        person.born_date = LocalDate.of( 1930+rnd.nextInt(80), 1+rnd.nextInt(12), 1+rnd.nextInt(27));
        person.age = Period.between(person.born_date, LocalDate.now()).getYears();
        return person;
    }

    // return не используется т.к. list это ссылочный объект
    public static void generateRandomArray(List<Person> list, int how_much){
        for(int i=0;i<how_much;i++)
            list.add(generate_Random());
    }

    // return не используется т.к. list это ссылочный объект
    public static void addDuplicates(List<Person> list, int how_much){
        for(int i=0;i<how_much;i++)
            list.add(list.get(rnd.nextInt(list.size())));
    }
}
