import java.util.List;
import java.util.Map;

/** Интерфейс содержащий требуемые методы сервиса */
public interface Services {

    /** Метод для реализации задания 1.
     * Определить существует ли хоть одно совпадение с шаблона в коллекции.*/
    boolean hasDuplicateOf(List<Person> list, Person p);

    /** Метод для реализации задания 2.
     * Вывести людей, у которых возраст не превышает 20 лет и фамилия начинается
     * на одну из букв (а,б,в,г,д) как в нижнем, так и верхнем регистре.*/
    List<Person> filteredByAgeLEQAndFirstLetterEQ(List<Person> list, int ageLEQ, char... chars );

    /** Метод для реализации задания 3.
     *  Вывести повторяющиеся объекты в следующем виде {1:{Объект человека}}
     *  в порядке убывания возраста человека. (1-количество повторяющихся элементов) */
    Map<Person,Long> groupedByNumberOfRepeatsAndSortedByAgeDescending(List<Person> list);

    /** Метод вывода данных для задания 2 */
    void show(List<Person> list);

    /** Метод вывода данных для задания 3 */
    void showWithRepeats(Map<Person,Long> list);
}
